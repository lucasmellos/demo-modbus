var modbus = require('./modbus_queue.js');

module.exports = {
    setScale: setScale,
    getScale: getScale,
    getTempetureOffset: getTempetureOffset,
    setTempetureOffset: setTempetureOffset,
    getSetpoint: getSetpoint,
    setSetpoint: setSetpoint,
    start: start,
    stop: stop,
    getTemp: getTemp,
    getInfo: getInfo   
};

function getScale(callback) {
    modbus.command('getScale', null, function(err, data) {
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            callback(null, {scale: data.data[0] == 1 ? 'fahrenheit' : 'celsius'});
        }
    });
}

function setScale(value, callback) {
    value = value.toUpperCase() == 'fahrenheit'.toUpperCase();
    modbus.command('setScale', value, function(err, data) {
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            callback(null, data);
        }
    });
}


function getTempetureOffset(callback) {
    modbus.command('getTempetureOffset', null, function(err, data) {
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            callback(null, data.data[0] / 10);
        }
    });
}

function setTempetureOffset(value, callback) {
    if (value >= -5 && value <= 5) {
        modbus.command('getTempetureOffset', value * 10, function(err, data) {
            if (err) {
                callback({
                    message: err.message,
                    strack: err.strack
                });
            } else {
                callback(null, data.data[0] / 10);
            }
        });
    } else {
        callback({
            message: '[set tempeture offset] Temperature is out of range. It should be between -5 and 5'
        });
    }
}

function getSetpoint(callback) {
    modbus.command('getSetpoint', function(err, data) {
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            callback(null, data.data[0] / 10);
        }
    });
}

function setSetpoint(value, callback) {
    if (value >= -30 && value <= 60) {
        modbus.command('setSetpoint', value * 10, function(err, data) {
            if (err) {
                callback({
                    message: err.message,
                    strack: err.strack
                });
            } else {
                callback(null, data);
            }
        });
    } else {
        callback({
            message: '[tempeture setpoint] Temperature is out of range. It should be between -30 and 60'
        });
    }
}


function start(callback) {
    control(1, callback);
}

function stop(callback) {
    control(0, callback);
}

function control(value, callback) {
    modbus.command('control', value, function(err, data) {
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            callback(null,data);
        }
    });
}

function getTemp(callback) {
    modbus.command('getTemp', null, function(err, data) {
        console.log(err,data);
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            var data = data.data;
            callback(null, {
                scale: (data[0] & 0x10) === 1 ? 'fahrenheit' : 'celsius',
                status: (data[0] & 0xF),
                meanValue: data[1] / 10,
                minValue: data[2] / 10,
                maxValue: data[3] / 10
            });
        }
    });
}

function getInfo(callback) {
    
    modbus.command('getInfo',null, function(err, data) {
        console.log(err,data);
        if (err) {
            callback({
                message: err.message,
                strack: err.strack
            });
        } else {
            var data = data.data;
            var str = String.fromCharCode((data[0] & 0xFF), (data[0] >> 8), (data[1] & 0xFF), (data[1] >> 8), 0, (data[2] & 0xFF), (data[2] >> 8), 0);
            callback(null, str);
        }
    });
}